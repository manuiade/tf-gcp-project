//-----------------------------------------------------------------------------
// variables.tf - contains the definition of variables used by Terraform
//-----------------------------------------------------------------------------

// Organization variables

variable "gcp_org_id" {
  type        = string
  description = "The GCP Organization ID"
}

variable "gcp_folder_id" {
  type        = string
  description = "The GCP parent Folder ID"
}

variable "gcp_ba_id" {
  type        = string
  description = "The GCP Billing Account ID"

  validation {
    condition     = can(regex("^([A-Z0-9]{6}-){2}[A-Z0-9]{6}$", var.gcp_ba_id))
    error_message = "Must be a valid GCP Billing Account ID."
  }
}

// Location variables

variable "gcp_region" {
  type        = string
  default     = "europe-west1"
  description = "The default GCP region to use"
}

variable "gcp_zone" {
  type        = string
  default     = "europe-west1-c"
  description = "The default GCP zone to use"
}

// Project variables

variable "project_setup" {
  description = "Contains definition of values for setup a project"
  type = object({
    id                    = string
    labels                = map(string)
    apis                  = list(string)
    service_account_name  = string
    service_account_roles = list(string)
  })
}