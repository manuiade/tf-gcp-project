//-----------------------------------------------------------------------------
// main.tf - contains the definition of resources created by Terraform
//-----------------------------------------------------------------------------

module "project" {
  source                = "./modules/project-setup"
  gcp_org_id            = var.gcp_org_id
  gcp_folder_id         = var.gcp_folder_id
  gcp_ba_id             = var.gcp_ba_id
  project_id            = var.project_setup.id
  project_labels        = var.project_setup.labels
  project_apis          = var.project_setup.apis
  service_account_name  = var.project_setup.service_account_name
  service_account_roles = var.project_setup.service_account_roles
}