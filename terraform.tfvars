// Organization variables
gcp_org_id    = "" # <ORG_ID>
gcp_folder_id = "" # You can leave it blank to create project on root level
gcp_ba_id     = "" #<BILLING_ACCOUNT_ID>

// Location variables
gcp_region = "europe-west1"
gcp_zone   = "europe-west1-c"

// Definition of GCP host projects with project labels, required APIs and service account IAM roles
project_setup = {
  id = "" # <PROJECT_ID>
  labels = {
    "kind" : "test"
  }
  apis = [
    "iam.googleapis.com",
    "servicenetworking.googleapis.com",
    "dns.googleapis.com",
    "networkmanagement.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com"
  ]
  service_account_name  = "project",
  service_account_roles = []
}