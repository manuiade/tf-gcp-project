//-----------------------------------------------------------------------------
// variables.tf - contains the definition of variables used by Terraform
//-----------------------------------------------------------------------------

variable "gcp_org_id" {
  type        = string
  description = "The GCP Organization ID"
}

variable "gcp_folder_id" {
  type        = string
  description = "The GCP parent Folder ID"
}

variable "gcp_ba_id" {
  type        = string
  description = "The GCP Billing Account ID"

  validation {
    condition     = can(regex("^([A-Z0-9]{6}-){2}[A-Z0-9]{6}$", var.gcp_ba_id))
    error_message = "Must be a valid GCP Billing Account ID."
  }
}

variable "project_id" {
  type        = string
  description = "The GCP Project ID"
}

variable "project_labels" {
  type        = map(string)
  default     = {}
  description = "Project level labels"
}

variable "project_apis" {
  type        = list(string)
  description = "List of GCP Project APIs to enable"
}

variable "service_account_name" {
  type        = string
  description = "Name of the project service account"
}

variable "service_account_roles" {
  type        = list(string)
  description = "List of IAM roles for the project service account"
}


