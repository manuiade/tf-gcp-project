//-----------------------------------------------------------------------------
// outputs.tf - contains the values to export to the root module
//-----------------------------------------------------------------------------

output "project_id" {
  value = google_project.project.project_id
}

output "project_number" {
  value = google_project.project.number
}

output "service_account_email" {
  value = google_service_account.project_sa.email
}
