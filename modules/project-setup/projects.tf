//-----------------------------------------------------------------------------
// projects.tf - creates GCP project, project lien to 
// prevent deletion and enable required apis
//-----------------------------------------------------------------------------


// Create GCP project
resource "google_project" "project" {
  name                = var.project_id
  project_id          = var.project_id
  folder_id           = local.parent_folder_id
  org_id              = local.parent_organization_id
  billing_account     = var.gcp_ba_id
  skip_delete         = false
  auto_create_network = false
  labels              = var.project_labels
}

// Lien to prevent project deletion
resource "google_resource_manager_lien" "prevent_deletion" {
  parent       = "projects/${google_project.project.number}"
  restrictions = ["resourcemanager.projects.delete"]
  origin       = "prevent-project-deletion"
  reason       = "Disable project lien before trying to delete project"
}

// Required APIs per project
resource "google_project_service" "project_apis" {
  count              = length(var.project_apis) > 0 ? length(var.project_apis) : 0
  service            = element(var.project_apis, count.index)
  project            = google_project.project.project_id
  disable_on_destroy = false
}
