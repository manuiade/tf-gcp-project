//-----------------------------------------------------------------------------
// service-account.tf - creates dedicated service account for the project
// assigning required IAM roles
//-----------------------------------------------------------------------------


// Service account
resource "google_service_account" "project_sa" {
  account_id   = var.service_account_name
  display_name = "Project Service Account"
  disabled     = false
  project      = google_project.project.project_id
  depends_on = [
    google_project_service.project_apis
  ]
}

// IAM roles assigned to project service accounts
resource "google_project_iam_member" "project_sa_roles" {
  count   = length(var.service_account_roles) > 0 ? length(var.service_account_roles) : 0
  project = google_project.project.project_id
  role    = element(var.service_account_roles, count.index)
  member  = format("serviceAccount:%s", google_service_account.project_sa.email)
}
