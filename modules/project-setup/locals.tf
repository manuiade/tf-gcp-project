//-----------------------------------------------------------------------------
// locals.tf - format variables for easier use in resource attributes
//-----------------------------------------------------------------------------

locals {
  parent_folder_id       = var.gcp_folder_id != "" ? var.gcp_folder_id : null
  parent_organization_id = var.gcp_folder_id != "" ? null : var.gcp_org_id
}