# Basic GCP Project creation with Terraform

This repository simply creates a GCP Project with a dedicated service account and enables some APIs.

Purpose of this repository is just to check the validity of a local Terraform installation respect to a GCP organization.

## Prerequisites

Ensure to have the following in place:

- A GCP Organization setup with an active Billing Account

- A GCP project with a private bucket and a service account with the following IAM roles at organization level:

```
roles/resourcemanager.organizationViewer
roles/orgpolicy.policyAdmin
roles/resourcemanager.folderAdmin
roles/resourcemanager.projectCreator
roles/billing.user
roles/storage.admin
roles/compute.xpnAdmin
roles/compute.networkAdmin
roles/browser
roles/resourcemanager.projectIamAdmin
```

- A dedicated Google account with the Service Account Token Creator role on the service account used by Terraform

- terraform >= 1.1.0 installed on your local machine

- gcloud installed on your local machine

## Setup authentication

Run the following to setup Terraform authentication to GCP using the service account

```
PROJECT_ID=<TF_PROJECT_ID>
SA_NAME=<TF_SA_NAME>

# OAuth Consent Screen
gcloud auth application-default login

# Setup impersonification of Terraform service account
export GOOGLE_IMPERSONATE_SERVICE_ACCOUNT=$SA_NAME@$PROJECT_ID.iam.gserviceaccount.com
```

## Launch Terraform

Change on *terraform.tfvars* and *providers.tf* the commented fields with your own.

From the repository root directoty simply launch:

```
terraform init
terraform plan -out plan.out
terraform apply plan.out
```

## Clean

From the repository root directoty simply launch:

```
terraform destroy --auto-approve
```