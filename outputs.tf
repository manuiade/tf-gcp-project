//-----------------------------------------------------------------------------
// outputs.tf - visualize result values after a terraform apply
//-----------------------------------------------------------------------------

output "project_id" {
  value = module.project.project_id
}

output "project_number" {
  value = module.project.project_number
}