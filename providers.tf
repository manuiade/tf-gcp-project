//-----------------------------------------------------------------------------
// proivders.tf - configuration of Terraform version, required providers,
// backend GCS bucket for storing Terraform state and default region/zone
//-----------------------------------------------------------------------------

terraform {
  backend "gcs" {
    bucket = "" # <TF_BUCKET_NAME>
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.12"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.12"
    }
  }
  required_version = "~> 1.1.0"
}

provider "google" {
  region = var.gcp_region
  zone   = var.gcp_zone
}

provider "google-beta" {
  region = var.gcp_region
  zone   = var.gcp_zone
}